import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/book_model.dart';

class ShowBook extends StatelessWidget {
  final id;
  ShowBook({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BOOK'),
      ),
      body: Center(
        child: FutureBuilder<List<BOOKModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<BOOKModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('ไม่มีข้อมูลที่บันทึกไว้'))
                  : ListView(
                      children: snapshot.data!.map((book) {
                        return Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 90,
                              ),
                              CircleAvatar(
                                backgroundImage: FileImage(File(book.image)),
                                radius: 150,
                              ),
                              SizedBox(
                                height: 60,
                              ),
                              Text(
                                '${book.name}',
                                style: TextStyle(
                                    fontSize: 26, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(' ${book.price}',
                                  style: TextStyle(
                                    fontSize: 18,
                                  )),
                            ],
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
