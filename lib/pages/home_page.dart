import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/data_model.dart';
import 'package:project_sqlite/model/book_model.dart';
import 'package:project_sqlite/pages/about.dart';
import 'package:project_sqlite/pages/add.dart';
import 'package:project_sqlite/pages/edit.dart';
import 'package:project_sqlite/pages/show.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int? selectedId;
  late Image personAccountImage;
  late Image accountHeaderImage;
  @override
  void initState() {
    super.initState();
    personAccountImage = Image.asset("assets/images/BOOK.png");
    accountHeaderImage = Image.asset("assets/images/PBOOK.jpg");
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(personAccountImage.image, context);
    precacheImage(accountHeaderImage.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(Icons.highlight_remove_rounded),
            onPressed: () {
              SystemNavigator.pop();
            },
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<List<BOOKModel>>(
            future: DatabaseHelper.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<BOOKModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('ไม่มีข้อมูลที่บันทึกไว้'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Center(
                          child: Card(
                            color: selectedId == grocery.id
                                ? Colors.white70
                                : Colors.white,
                            child: ListTile(
                              title: Text('${grocery.name} '),
                              subtitle: Text('${grocery.price} '),
                              leading: CircleAvatar(
                                  backgroundImage:
                                      FileImage(File(grocery.image))),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit_road),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditBook(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.delete_forever),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "คุณต้องการลบบันทึกนี้หรือไม่?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("ใช่"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("ไม่"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShowBook(id: profileid)));

                                setState(() {
                                  print(grocery.image);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  DatabaseHelper.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(83, 141, 83, 1.0),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: const Text('บันทึกข้อมูล '),
                accountEmail: const Text(' by BOBO'),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: personAccountImage.image,
                ),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: accountHeaderImage.image, fit: BoxFit.cover)),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'BOOK',
                icon: Icons.book,
                onClicked: () => selectedItem(context, 0),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'AddNote',
                icon: Icons.note_add_outlined,
                onClicked: () => selectedItem(context, 1),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'About',
                icon: Icons.person,
                onClicked: () => selectedItem(context, 2),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'Close',
                icon: Icons.close,
                onClicked: () => selectedItem(context, 0),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;
      case 1:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddBook()),
        );
        break;
      case 2:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const About()),
        );
        break;
    }
  }
}
