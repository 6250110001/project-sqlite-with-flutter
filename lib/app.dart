import 'package:flutter/material.dart';
import 'package:project_sqlite/pages/add.dart';
import 'package:project_sqlite/pages/home_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BOOK',
      debugShowCheckedModeBanner : false,
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: const MyHomePage(title: 'BOOK'),
    );
  }
}
