class BOOKModel {
  int? id;
  String name;
  String price;
  String image;

  BOOKModel({
    this.id,
    required this.name,
    required this.price,
    required this.image,
  });

  factory BOOKModel.fromMap(Map<String, dynamic> json) => new BOOKModel(
        id: json['id'],
        name: json['name'],
    price: json['price'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'image': image,
    };
  }
}
